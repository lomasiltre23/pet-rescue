import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private options = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }
  constructor(private http: HttpClient) { }

  public get(url: string): Observable<any> {
    return this.http.get<any>(url, this.options)
      .pipe(map((res: any) => res))
      .pipe(catchError((error:any) => Observable.throw(error || 'Server error')));
  }
  public post(url: string, body: Object): Observable<any> {
    return this.http.post<any>(url, body, this.options)
      .pipe(map((res: any) => res))
      .pipe(catchError((error:any) => Observable.throw(error || 'Server error')));
  }

  public delete(url: string){
    return this.http.delete(url)
      .pipe(map((res: any) => res))
      .pipe(catchError((error:any) => Observable.throw(error || 'Server error')));

  }
}
