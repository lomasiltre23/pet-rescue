import { Component, OnInit } from '@angular/core';
import { HttpService} from 'src/app/services/http.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.less']
})
export class FriendsComponent implements OnInit {
  public animals: any[] = [];
  constructor(private http: HttpService) { }

  ngOnInit() {
    this.getAllAnimals();
  }

  getAllAnimals(){
    this.http.get("http://localhost:3000/animals").subscribe( res => {
      this.animals = res;
    }, error => {
      console.error(error);
      
    })
  }

  createNewFriend(form: NgForm){
    let body = form.value;
    this.http.post("http://localhost:3000/animals", body).subscribe( res => {
      this.animals.push(res);
      form.reset();
    },error => {
      console.error(error);
    });
  }

  removeAnimal(data){
    let index = this.animals.findIndex(value => value._id == data._id);
    this.animals.splice(index, 1);
    console.log(index);
  }

}
