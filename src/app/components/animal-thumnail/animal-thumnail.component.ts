import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'animal-thumnail',
  templateUrl: './animal-thumnail.component.html',
  styleUrls: ['./animal-thumnail.component.less']
})
export class AnimalThumnailComponent implements OnInit {
  @Input("info") data: any;
  @Output("OnDelete") deleteEvent = new EventEmitter<any>();
  constructor(private http: HttpService) { }

  ngOnInit() {
  }

  deleteFriend(){
    this.http.delete("http://localhost:3000/animals/" + this.data._id).subscribe(res => {
      this.deleteEvent.emit(this.data);
    }, error => {
      console.error(error);
    });
  }

}
