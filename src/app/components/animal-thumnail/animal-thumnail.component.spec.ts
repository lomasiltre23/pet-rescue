import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalThumnailComponent } from './animal-thumnail.component';

describe('AnimalThumnailComponent', () => {
  let component: AnimalThumnailComponent;
  let fixture: ComponentFixture<AnimalThumnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalThumnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalThumnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
