import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.less']
})
export class NavBarComponent implements OnInit {
  constructor(private route: Router) { }

  ngOnInit() {
  }

  isActive(route): boolean { return (this.route.url === route) }
}
